# Slippers UI - GitLab's Marketing Design System

## Gettings started:
### Dependencies
- Node - Check .nvmrc for the current node version.

### Local development
Slippers uses [Storybook](https://storybook.js.org/) to enable component exploration.

1. `npm install` - Installs dependencies
2. `npm run storybook` - Runs Storybook


## Updating the slippers-ui NPM package
1. Make changes to slippers-ui.
2. If needed, create/update .stories so Storybook is upto date.
3. Increment the version number in package.json.
4. If needed, add component into install.js (this adds components into the build)
5. Build /dist folder - `npm run build library`.
6. Publish to NPM - `npm publish`.
7. Merge changes into origin/main.
8. Once the package has been published it's now ready to be updated in the consuming repositories (Example: Buyer Experience, be-navigation, etc.).

## Installation
Slippers is available as an npm package.

    // with npm
    npm install be-navigation

    // with yarn
    yarn add be-navigation
