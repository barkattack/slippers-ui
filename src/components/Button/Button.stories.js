import SlpButton from "./Button.vue";

export default {
  title: "Components/Button",
  component: SlpButton,
  argTypes: {
    variant: {
      control: { type: "select" },
      options: ["primary", "secondary", "tertiary", "ghost"],
    },
  },
};

const Template = (args, { argTypes }) => {
  return {
    components: { SlpButton },
    props: Object.keys(argTypes),
    template: `<SlpButton v-bind="$props">Button</SlpButton>`,
  };
};

export const Default = Template.bind({});
Default.args = {
  variant: "primary",
  disabled: false,
  href: "",
};
