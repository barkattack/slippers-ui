import SlpButton from "./Button.vue";
import SlpIcon from "../Icon/Icon.vue";

// TODO: Was too lazy to put this in the Button.stories.vue file
export default {
  title: "Components/Button(IconVariant)",
  component: {
    SlpButton,
    SlpIcon,
  },
  argTypes: {
    variant: {
      control: { type: "select" },
      options: ["icon"],
    },
  },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => {
  return {
    components: { SlpButton, SlpIcon },
    props: Object.keys(argTypes),
    template: `<SlpButton v-bind="$props">Button<SlpIcon variant="product" name="chevron-lg-right"
    /></SlpButton>`,
  };
};

export const Default = Template.bind({});
Default.args = {
  variant: "icon",
  disabled: false,
};
