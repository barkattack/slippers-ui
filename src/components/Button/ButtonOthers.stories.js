import SlpTopButton from "./TopButton.vue";
import SlpPillButton from "./PillButton.vue";
import SlpGroupButton from "./GroupButton.vue";

export default {
  title: "Components/Buttons (variants)",
  argTypes: {
    iconVariant: {
      control: { type: "select", options: ["product", "marketing"] },
    },
    type: {
      control: { type: "select", options: ["true", "false", "label"] },
    },
  },
};

// ToTopButton
const TemplateToTop = (args, { argTypes }) => {
  return {
    components: { SlpTopButton },
    props: Object.keys(argTypes),
    template: `
    <div style="height: 3000px">
    Scroll down to reveal the button
      <SlpTopButton /> 
    </div>`,
  };
};
export const TopButton = TemplateToTop.bind({});

/* PillButton */
const TemplatePill = (args, { argTypes }) => {
  return {
    components: { SlpPillButton },
    props: Object.keys(argTypes),
    template: `
    <div style="padding: 50px">
      <SlpPillButton v-bind="$props">
        {{text}}
      </SlpPillButton>

      <div style="height: 30px"/>

      <SlpPillButton type="true" :href="href"  style="margin: 10px">
        {{text}}
      </SlpPillButton> 
      <SlpPillButton type="false" :href="href"  style="margin: 10px">
        {{text}}
      </SlpPillButton> 
      <SlpPillButton type="label" :href="href"  style="margin: 10px">
        {{text}}
      </SlpPillButton>  
    </div>`,
  };
};
export const PillButton = TemplatePill.bind({});
PillButton.args = {
  type: "true",
  text: "Text",
  icon: "check",
  iconVariant: "product",
  href: "",
};

/* GroupButton */
const TemplateGroup = (args, { argTypes }) => {
  return {
    components: { SlpGroupButton },
    props: Object.keys(argTypes),
    template: `
    <div style="padding: 50px">
      <SlpGroupButton v-bind="$props">
        {{text}}
      </SlpGroupButton> 
    </div>`,
  };
};
export const GroupButton = TemplateGroup.bind({});
GroupButton.args = {
  text: "Default text",
  iconLeft: "continuous-delivery",
  iconRight: "arrow-down",
  href: "",
};
