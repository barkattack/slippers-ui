import SlpDropdown from "./Dropdown.vue";
import SlpDropdownTest from "./DropdownTest.vue";

export default {
  title: "Components/Dropdown",
  component: SlpDropdown,
  SlpDropdownTest,
  argTypes: {},
};

const Template = (args, { argTypes }) => ({
  components: { SlpDropdown },
  props: Object.keys(argTypes),
  template: `<SlpDropdown v-bind="$props"></SlpDropdown>`,
});

const PressureTest = (args, { argTypes }) => ({
  components: { SlpDropdownTest },
  props: Object.keys(argTypes),
  template: `<SlpDropdownTest v-bind="$props"></SlpDropdownTest>`,
});

export const NoProps = Template.bind({});
NoProps.args = {
  name: "no-props",
};

export const WithProps = Template.bind({});
WithProps.args = {
  name: "with-props",
  options: ["Sales", "Engineering", "Marketing", "Legal"],
  value: "Marketing",
};

export const Test = PressureTest.bind({});

/**
<script lang="ts">
import Vue from 'vue'
import SlpDropdown from "./Dropdown.vue";
import SlpCard from '../Card/Card.vue';


export default Vue.extend({
  components: {
    SlpDropdown, SlpCard
  },
  data(){
    return {
      show: 'Option 1'
    }
  },
  methods: {
    resolveEvent(event){
      this.show = event
    }
  }
})
</script>

<template>
    <div>
      <SlpDropdown name="filter" @filter="resolveEvent"></SlpDropdown>
      <SlpCard v-show="show==='Option 1'">1</SlpCard>
      <SlpCard v-show="show==='Option 2'">2</SlpCard>
      <SlpCard v-show="show==='Option 3'">3</SlpCard>
  </div>
</template>
*/
