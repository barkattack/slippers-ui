import SlpToggle from "./Toggle.vue";

export default {
  title: "Components/Toggle",
  component: SlpToggle,
  argTypes: {},
};

const Template = (args, { argTypes }) => {
  return {
    components: { SlpToggle },
    props: Object.keys(argTypes),
    template: `
    <div style="padding: 50px">
      <SlpToggle v-bind="$props" /> 
    </div>`,
  };
};

export const Toggle = Template.bind({});
Toggle.args = {
  trueText: "Monthly",
  falseText: "Weekly",
};
