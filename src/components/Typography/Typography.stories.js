import SlpTypography from "./Typography.vue";

export default {
  title: "Components/Typography",
  component: SlpTypography,
  argTypes: {
    variant: {
      control: { type: "select" },
      options: [
        "display1",
        "heading1",
        "heading2",
        "heading3",
        "heading4",
        "heading5",
        "heading1-bold",
        "heading2-bold",
        "heading3-bold",
        "heading4-bold",
        "heading5-bold",
        "body1",
        "body2",
        "body3",
        "body1-bold",
        "body2-bold",
        "body3-bold",
        "all-caps",
        "arrow",
        "quotes",
        "emphasis",
      ],
    },
    tag: {
      control: { type: "select" },
      options: ["span", "h1", "h2", "h3", "h4", "p", "div"],
    },
  },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: { SlpTypography },
  props: Object.keys(argTypes),
  template:
    '<SlpTypography v-bind="$props">Lorem ipsum dolor sit amet</SlpTypography>',
});

export const Default = Template.bind({});
Default.args = {
  variant: "heading1",
};
