import SlpRadioGroup from "./RadioGroup";

export default {
  title: "Components/RadioGroup",
  component: SlpRadioGroup,
  argTypes: {
    orientation: {
      control: { type: "select" },
      options: ["vertical", "horizontal"],
    },
  },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: { SlpRadioGroup },
  props: Object.keys(argTypes),
  template: '<SlpRadioGroup v-bind="$props">RadioGroup</SlpRadioGroup>',
});

export const Default = Template.bind({});
Default.args = {
  options: ["first", "second", "third", "fourth"],
};
