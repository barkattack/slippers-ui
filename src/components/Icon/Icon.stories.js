import Icon from "./Icon";

const requireIcons = require.context("../../static/icons/", false, /.svg$/);

const availableIcons = requireIcons
  .keys()
  .map((fileName) => fileName.replace(/^\.\/(.+)\.svg$/, "$1"));

export default {
  title: "Components/Icon",
  component: Icon,
  argTypes: {
    name: {
      control: { type: "select" },
      options: availableIcons,
    },
    size: {
      control: { type: "select" },
      options: ["xs", "sm", "md", `lg`, "xl"],
    },
    variant: {
      control: { type: "select" },
      options: ["product", "marketing"],
    },
  },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: { Icon },
  props: Object.keys(argTypes),
  template: '<Icon v-bind="$props">Icon</Icon>',
});

export const Default = Template.bind({});
Default.args = {
  name: "accelerate",
};
