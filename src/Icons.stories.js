import Icon from "./components/Icon/Icon";
import Container from "./components/Container/Container";
import Column from "./components/Column/Column";

const requireIcons = require.context("./static/icons/", false, /.svg$/);
const fullIconList = requireIcons
  .keys()
  .map((fileName) => fileName.replace(/^\.\/(.+)\.svg$/, "$1"));
const sizeOptions = ["xs", "sm", "md", `lg`, "xl"];

export default {
  title: "Foundations/Icons",
};

const Template = (args, { argTypes }) => ({
  components: { Icon, Container, Column },
  props: Object.keys(argTypes),
  methods: {
    filterIcon(input) {
      this.iconList = fullIconList.filter((name) =>
        name.includes(input.target.value)
      );
    },
  },
  data() {
    return {
      iconList: fullIconList,
      sizeOptions,
    };
  },
  template: `
    <section class='slp-storybook-icon-gallery slp-mt-64'>
      <Container>
        <div class='slp-mx-16 slp-mb-32'>
          <h1 class='slp-mb-32'>Icon Gallery</h1>
          <p class='slp-mb-16'>Complete list of icons that can be used with the <strong>marketing variant</strong>.</p>
          <p class='slp-mb-16'>For the <strong>product variant</strong> check <a
            href='https://gitlab-org.gitlab.io/gitlab-svgs/' target='_blank'>https://gitlab-org.gitlab.io/gitlab-svgs/</a>
          </p>
        </div>
        <div class='slp-mx-16'>
          <input type='text' class='slp-storybook-icon-gallery__input' placeholder='Filter icons...' @input='filterIcon' />
        </div>
        <div class='slp-storybook-icon-gallery__body'>
          <Column :cols='4' v-for='icon in iconList'>
            <div class='slp-storybook-icon-gallery__icon-card'>
              <h5 class='slp-mb-8'>{{ icon }}</h5>
              <div class='slp-storybook-icon-gallery__icons'>
                <div v-for='size in sizeOptions' class='slp-mx-8'>
                  <Icon :name='icon' variant='marketing' :size='size' class='slp-mb-8' />
                  <p>{{ size }}</p>
                </div>
              </div>
            </div>
          </Column>
        </div>
      </Container>
    </section>
  `,
});

export const Gallery = Template.bind({});
